#!/usr/bin/env python
from flask import Flask
from flask import redirect
from flask import render_template
from flask import request

from flask_migrate import Migrate

from db import db

import config

app = Flask(__name__)
app.config.from_object(config.Development)

db.init_app(app)
migrate = Migrate(app, db)

from models.log import Log

@app.route('/')
@app.route('/home')
def home():
    logs = Log.query.order_by(Log.id.desc()).all()
    return render_template('home.html', logs=logs)

@app.route('/add', methods=['POST'])
def add_log():
    description = request.form['description']

    log = Log(description)
    db.session.add(log)
    db.session.commit()

    return redirect('/')

@app.route('/delete/<int:log_id>')
def delete_log(log_id):
    log = Log.query.get(log_id)

    if not log:
        return redirect('/')

    db.session.delete(log)
    db.session.commit()

    return redirect('/')

@app.errorhandler(403)
def forbidden(error):
    return render_template('403.html'), 403

@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404

@app.errorhandler(405)
def method_not_allowed(error):
    return render_template('405.html'), 405

@app.errorhandler(500)
def internal_server_error(error):
    return render_template('500.html'), 500

if __name__ == '__main__':
    app.run(host=config.Development.HOST, port=config.Development.PORT)
