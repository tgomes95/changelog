from datetime import datetime

from db import db

class Log(db.Model):

    __tablename__ = 'logs'

    id          = db.Column(db.Integer, autoincrement=True, primary_key=True)
    description = db.Column(db.Text, nullable=False)
    created_at  = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)

    def __init__(self, description):
        self.created_at  = datetime.utcnow()
        self.description = description

    def json(self):
        return {
            'created_at':  str(self.created_at),
            'description': self.description
        }

    @property
    def created_at_str(self):
        return self.created_at.strftime('%d.%m.%Y %H:%M:%S')
